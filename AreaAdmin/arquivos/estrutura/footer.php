</div>
<div class="clear"></div>
<footer id="admFoo">
    <div id="itensRod">
        <div>
            <h6>E-mail</h6>
            <p>atb@atb.com</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Telefone</h6>
            <p> (53) 9999-9999</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Cidade</h6>
            <p>Bagé</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Social</h6>
            <span class="fa-stack fa-lg"><a href="#" target="_blank">
                    <i class="fa fa-facebook-square fa-2x"></i></a>
            </span>
            <span class="fa-stack fa-lg"><a href="#" target="_blank">
                    <i class="fa fa-twitter-square fa-2x"></i></a>
            </span>
            <span class="fa-stack fa-lg"><a href="#" target="_blank">
                    <i class="fa fa-linkedin-square fa-2x"></i></a>
            </span>
            <p>@2016 - Todos direitos reservados.</p>
        </div>
    </div>
</footer>
</body>
</html>

<link href="../arquivos/css/normal/main.css" rel="stylesheet" type="text/css"/>
<script src="../arquivos/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../arquivos/js/main.js" type="text/javascript"></script>
<link href="../arquivos/css/normal/fonts.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">