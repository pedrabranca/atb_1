/* SCRIPT DA SENHA */
jQuery(document).ready(function () {
    "use strict";
    var options = {};
    options.ui = {
        container: "#pwd-container",
        showStatus: true,
        showProgressBar: false,
        viewports: {
            verdict: ".pwstrength_viewport_verdict"
        }
    };
    $('#senha').pwstrength(options);
});
//FUNÇÕES DAS MÁSCARAS
function Mascara(o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()", 1);
}
function execmascara() {
    v_obj.value = v_fun(v_obj.value);
}
//MÁSCARAS
function Telefone(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function Cpf(v) {
    v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return v;
}
function Cep(v) {
    v = v.replace(/\D/g, "").replace(/^(\d{5})(\d)/, "$1-$2");
    return v;
}
$(document).ready(function () {
    var nome = false;
    var email = false;
    var tel = false;
    var cpf = false;
    var cep = false;
    var senha = false;
    var cfSenha = false;
    var nasc = false;
    var sexo = false;
//CAMPOS PREENCHIDOS SUCESSO/FALHA
    function NomeErro() {
//      $( "#smalNome" ).css( "display", "block" );
        $('#nomeDiv').addClass('has-error');
        $('#nomeDiv').removeClass('has-success');
        $('#logoNome').removeClass('fa-check');
        $('#logoNome').addClass('fa-times');
        nome = false;
        testaForm();
    }
    function NomeSucesso() {
        $('#nomeDiv').removeClass('has-error');
        $('#nomeDiv').addClass('has-success');
        $('#logoNome').removeClass('fa-times');
        $('#logoNome').addClass('fa-check');
        nome = true;
        testaForm();
    }
    function EmailErro() {
        $('#emailDiv').addClass('has-error');
        $('#emailDiv').removeClass('has-success');
        $('#logoMail').removeClass('fa-check');
        $('#logoMail').addClass('fa-times');
        //e-mail tem verificação por máscara
        $("#mailmsg").attr('data-hint', 'Seu e-mail precisa estar no formato: nome@email.com');
        email = false;
        testaForm();
    }
    function EmailSucesso() {
        $('#emailDiv').removeClass('has-error');
        $('#emailDiv').addClass('has-success');
        $('#logoMail').removeClass('fa-times');
        $('#logoMail').addClass('fa-check');
        $("#mailmsg").attr('data-hint', '');
        email = true;
        testaForm();
    }
    function TelErro() {
        $('#telDiv').addClass('has-error');
        $('#telDiv').removeClass('has-success');
        $('#logoTel').removeClass('fa-check');
        $('#logoTel').addClass('fa-times');
        tel = false;
        testaForm();
    }
    function TelSucesso() {
        $('#telDiv').removeClass('has-error');
        $('#telDiv').addClass('has-success');
        $('#logoTel').removeClass('fa-times');
        $('#logoTel').addClass('fa-check');
        $("#telmsg").attr('data-hint', '');
        tel = true;
        testaForm();
    }
    function CpfErro() {
        $('#cpfDiv').addClass('has-error');
        $('#cpfDiv').removeClass('has-success');
        $('#logoCpf').removeClass('fa-check');
        $('#logoCpf').addClass('fa-times');
        cpf = false;
        testaForm();
    }
    function CpfSucesso() {
        $('#cpfDiv').removeClass('has-error');
        $('#cpfDiv').addClass('has-success');
        $('#logoCpf').removeClass('fa-times');
        $('#logoCpf').addClass('fa-check');
        $("#cpfmsg").attr('data-hint', '');
        cpf = true;
        testaForm();
    }
    function CepErro() {
        $('#cepDiv').addClass('has-error');
        $('#cepDiv').removeClass('has-success');
        $('#logoCep').removeClass('fa-check');
        $('#logoCep').addClass('fa-times');
        cep = false;
        testaForm();
    }
    function CepSucesso() {
        $('#cepDiv').removeClass('has-error');
        $('#cepDiv').addClass('has-success');
        $('#logoCep').removeClass('fa-times');
        $('#logoCep').addClass('fa-check');
        $("#cepmsg").attr('data-hint', '');
        cep = true;
        testaForm();
    }
    function CfSenhaErro() {
        $('#CfsenhaDiv').addClass('has-error');
        $('#CfsenhaDiv').removeClass('has-success');
        $('#logoCfsenha').removeClass('fa-check');
        $('#logoCfsenha').addClass('fa-times');
        cfSenha = false;
        testaForm();
    }
    function CfSenhaSucesso() {
        $('#CfsenhaDiv').removeClass('has-error');
        $('#CfsenhaDiv').addClass('has-success');
        $('#logoCfsenha').removeClass('fa-times');
        $('#logoCfsenha').addClass('fa-check');
        cfSenha = true;
        testaForm();
    }
    function NascimentoErro() {
        $('#nascimentoDiv').addClass('has-error');
        $('#nascimentoDiv').removeClass('has-success');
        $('#logoNascimento').removeClass('fa-check');
        $('#logoNascimento').addClass('fa-times');
        nasc = false;
        testaForm();
    }
    function NascimentoSucesso() {
        $('#nascimentoDiv').removeClass('has-error');
        $('#nascimentoDiv').addClass('has-success');
        $('#logoNascimento').removeClass('fa-times');
        $('#logoNascimento').addClass('fa-check');
        nasc = true;
        testaForm();
    }
    function SexoSucesso() {
        $('#sexo').addClass('has-success');
        $('#sb').addClass('bordNone');
        sexo = true;
        testaForm();
    }
    function testaForm() {
        if (nome == true && email == true && tel == true && cpf == true && cep == true
                && senha == true && cfSenha == true && nasc == true && sexo == true) {
            liberaForm();
        } else {
            blockForm();
        }
    }
    function liberaForm() {
        $('#enviar').prop('disabled', false);
    }
    function blockForm() {
        $('#enviar').prop('disabled', true);
    }
    function confirmaSen() {
        var CfsenhaOri = $('#senha').val();
        var Cfsenha = $('#Cfsenha').val();
        if (CfsenhaOri == Cfsenha) {
            CfSenhaSucesso();
        } else {
            CfSenhaErro();
        }
    }
//VALIDACAO DOS ELEMENTOS
    $('#nome').on('keyup blur change paste', function (e) {
        if ($("#nome").val().length < 2) {
            NomeErro();
        } else {
            NomeSucesso();
        }
    });
    $('#email').on('keyup blur change paste', function (e) {
        var regexEmail = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/;
        if (!regexEmail.test($('#email').val())) {
            EmailErro();
        } else {
            EmailSucesso();
        }
        $('#email').blur(function () {
            $("#mailmsg").attr('data-hint', '');
        });
    });
    $('#tel').on('keyup blur change paste', function (e) {
        Mascara(this, Telefone);
        var telefone = $("#tel").val().toString();
        telefone = telefone.replace(/[^0-9]/g, '');
        var tzero = telefone.indexOf('0000000');
        var tum = telefone.indexOf('1111111');
        var tdois = telefone.indexOf('2222222');
        var ttres = telefone.indexOf('3333333');
        var tquatro = telefone.indexOf('4444444');
        var tcinco = telefone.indexOf('5555555');
        var tseis = telefone.indexOf('6666666');
        var tsete = telefone.indexOf('7777777');
        var toito = telefone.indexOf('8888888');
        var tnove = telefone.indexOf('9999999');
        if (telefone.length < 10 || tum >= 0 || tdois >= 0 || ttres >= 0 || tquatro >= 0 ||
                tcinco >= 0 || tseis >= 0 || tsete >= 0 || toito >= 0 || tnove >= 0 || tzero >= 0) {
            if (telefone.length < 10) {
                $("#telmsg").attr('data-hint', 'Precisa ter pelo menos 10 números.');
            } else {
                $("#telmsg").attr('data-hint', 'Insira um número de telefone verdadeiro.');
            }
            TelErro();
        }
        else {
            TelSucesso();
        }
        $('#tel').blur(function () {
            $("#telmsg").attr('data-hint', '');
        });
    });
    $('#cpf').on('keyup blur change paste', function (e) {
        Mascara(this, Cpf);
        var input_cpf = $("#cpf").val();
        function validarCPF(input_cpf) {
            if (input_cpf) {
                var inputCpf = input_cpf.toString();
                inputCpf = inputCpf.replace(/[^0-9]/g, '');
                var czero = inputCpf.indexOf('0000000');
                var cum = inputCpf.indexOf('1111111');
                var cdois = inputCpf.indexOf('2222222');
                var ctres = inputCpf.indexOf('3333333');
                var cquatro = inputCpf.indexOf('4444444');
                var ccinco = inputCpf.indexOf('5555555');
                var cseis = inputCpf.indexOf('6666666');
                var csete = inputCpf.indexOf('7777777');
                var coito = inputCpf.indexOf('8888888');
                var cnove = inputCpf.indexOf('9999999');
                if (inputCpf.length != 11) {
                    CpfErro();
                    $("#cpfmsg").attr('data-hint', 'O cpf precisa ter 11 dígitos.');
                } else {
                    var numeros = [];
                    var pesos_A = [10, 9, 8, 7, 6, 5, 4, 3, 2];
                    var pesos_B = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2];
                    var sum = 0;
                    var x1 = 0;
                    var x2 = 0;
                    for (var i = 0; i < 9 && i < inputCpf.length; i++) {
                        var digito = inputCpf[i]
                        sum = sum + digito * pesos_A[i];
                    }
                    //calcula digito 1
                    var mod = sum % 11;
                    if (mod >= 2) {
                        x1 = 11 - mod;
                    }
                    //calcula digito 2
                    sum = 0;
                    for (var i = 0; i < 10 && i < inputCpf.length; i++) {
                        sum = sum + inputCpf[i] * pesos_B[i];
                    }
                    var mod = sum % 11;
                    if (mod >= 2) {
                        x2 = 11 - mod;
                    }
                    if (x1 == inputCpf[9] && x2 == inputCpf[10] && cum == -1 && cdois == -1 && ctres == -1 && cquatro == -1 && ccinco == -1
                            && cseis == -1 && csete == -1 && coito == -1 && cnove == -1 && czero == -1) {
                        CpfSucesso();
                    } else {
                        $("#cpfmsg").attr('data-hint', 'Cpf inválido.');
                        CpfErro();
                    }
                }
            } else {
                CpfErro();
            }
        }
        ;
        validarCPF(input_cpf);
    });
    $('#cep').on('keyup change paste', function (e) {
        Mascara(this, Cep);
    });
    $('#senha').on('keyup blur change paste', function (e) {
        confirmaSen();
        var pwd = $('#pwd').val();
        if (pwd == 0) {
            senha = false;
            testaForm();
            return senha;
        }
        if (pwd == 1) {
            testaForm();
            senha = true;
            return senha;
        }
        else {
            senha = false;
            testaForm();
            console.log(pwd);
            console.log(senha);
            console.log("nem 0 nem 1");
            return senha;
        }
    });
    $('#Cfsenha').on('keyup blur change paste', function (e) {
        confirmaSen();
    });
    $('#nascimento').on('keyup blur change paste', function (e) {
        var data = $("#nascimento").val();
        if (data !== '') {
            var input = document.getElementById('nascimento').value;
            var d = new Date(input);
            if (!!d.valueOf()) { // Valid date
                year = d.getFullYear();
                month = d.getMonth();
                day = d.getDate();
                var currentYear = (new Date).getFullYear();
                if (year > 1800 && month >= 0 && year <= currentYear && month < 13 && day > 0 && day < 33) {
                    NascimentoSucesso();
                } else {
                    NascimentoErro();
                }
            }
            else { /* Invalid date */
                NascimentoErro();
            }
        } else {
            NascimentoErro();
        }
    });
    $("input[name='sexo']").on("change", function () {
        $("input[name='sexo']").not($(this)).prop('checked', false);
        SexoSucesso();
    });
// ----------SCRIPT DO CEP----------
    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
    }
//Quando o valor do cep chega a 9
    $("#cep").on('keyup keypress blur change paste', function (e) {
        var len = this.value.length;
        if (len === 9) {
//Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            //Verifica se campo cep possui valor informado.
            if (cep !== "") {
//Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;
                //Valida o formato do CEP.
                if (validacep.test(cep)) {
//Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");
                    //Consulta o webservice viacep.com.br
                    $.getJSON("http://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                            CepSucesso();
                        } //end if.
                        else {
                            $("#cepmsg").attr('data-hint', 'Cep não encontrado.');
                            limpa_formulário_cep();
                            CepErro();
                        }
                    });
                } //end if.
            } //end if.
        } else {
            $("#cepmsg").attr('data-hint', 'O cep precisa ter 8 dígitos.');
            CepErro();
            limpa_formulário_cep();
        }
        $('#cep').on('blur', function (e) {
            $("#cepmsg").attr('data-hint', '');
        });
    }
    );
    $('#alt_se').click(function () {
        show('senha');
        show('CfsenhaDiv');
        hide('alt_se');
    });
    //verifica formulario onload nas paginas de editar cadastro
    var modifica = $('#modifica').val();
    if (modifica == 1) {
        senha = true;
        NomeSucesso();
        EmailSucesso();
        TelSucesso();
        CpfSucesso();
        CepSucesso();
        CfSenhaSucesso();
        NascimentoSucesso();
        SexoSucesso();
        //kkk
        //alert("tesete");
    }
    $('#cadastra').submit(function () {
        if (grecaptcha.getResponse() == '') {
            swal({
                title: "Último detalhe!",
                text: "Você precisa confirmar que não é um robô antes de enviar o formulário!",
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: 'Ok!'
            });
            event.preventDefault();
        } else {
//swal("\nSeu cadastro foi realizado com sucesso!", "", "success");
        }
    });
    $('#cancelar').click(function () {
        swal({
            title: "Você tem certeza que quer cancelar?",
            text: "Você irá perder todos os dados preenchidos até agora!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Sim!",
            closeOnConfirm: true
        },
        function () {
            show('clickMeId');
            hide('formulario');
        });
    });
    $('.excluir').click(function () {
        event.preventDefault();
        var exc = this;
        swal({
            title: "Você tem certeza que deseja apagar este funcionário do sistema?",
            text: "Esta decisão é permanente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Sim!",
            closeOnConfirm: true
        },
        function () {
            //swal("Funcionario apagado com sucesso!", "", "success");
            //show('clickMeId');
            //hide('formulario');
            $(exc).trigger('click');
        });
    });
});
// DEMAIS FUNÇÕES
function show(toBlock) {
    setDisplay(toBlock, 'block');
}
function hide(toNone) {
    setDisplay(toNone, 'none');
}
function setDisplay(target, str) {
    document.getElementById(target).style.display = str;
}