<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        background: red;
        cursor: inherit;
        display: block;
    }
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>
<h1>Escritorio</h1>
<div id="clickMeId">
    <a class="btn btn-primary btn-large adicionar" onclick="show('formulario');
            hide('clickMeId')">
        Adicionar
    </a>
</div>


<div id="formulario" style="display:none;">
    <div class="container jumbotron">
        <div style="float: right;" id="clickMeId2" onclick="show('clickMeId');
                hide('formulario')">
            <i style="color:#981C19;" class="fa fa-times fa-2x"></i>
        </div>
        <h2 class="text-center">Cadastro de Itens</h2>
        <div class="alert alert-danger"></div>
        <hr>
        <form class="form-horizontal" method="POST" action="grava-escritorio.php" data-toggle="validator" id="contactForm" role="form" enctype="multipart/form-data">

            <div class="form-group">
                <div class="btn btn-info" id="add">Adicionar imagem</div>
                <input type="hidden" name="nro_img" id="nro_img">
            </div>
            <div id="imagens">

            </div>
            <hr>
            <!--<div class="form-group">
                <textarea rows="4" style="width:400px;" name="descricao" id="descricao" placeholder="Informações Adicionais (opcional)." maxlength="240"></textarea>
            </div>-->


            <div class='nav'>
                <button class='btn btn-danger' onclick="show('clickMeId');
                        hide('formulario')" type="button" id="contactForm">Cancelar</button>

                <button type="submit" id="contactForm" class="btn btn-success">Enviar</button>
            </div>
        </form>
    </div>
</div>

<?php
include_once '../includes/model/dao/bd2.php';

$pdo = conecta();

$busca = $pdo->prepare("SELECT * FROM img_escritorio order by id desc;");
$busca->execute();
while ($linha = $busca->fetch(PDO::FETCH_ASSOC)) {
    $caminho = '../arquivos/img/escritorio/';
    ?>


    <div class='row'>
        <div class='col-md-6 col-md-offset-3' style='margin-top:20px;'>
            <img src="<?= $caminho . $linha['img1'] ?>" style='width:100%; height: 400px; border: 2px solid black;'><button class='btn btn-danger btn-img' onclick="javascript:window.location.href = 'deleta-escritorio.php?id=<?= $linha['img1'] ?>'">Excluir</button>
        </div>
    </div><br />
    <?php
}
?>
<br /><br />

<script>
    function show(toBlock) {
        setDisplay(toBlock, 'block');
    }
    function hide(toNone) {
        setDisplay(toNone, 'none');
    }
    function setDisplay(target, str) {
        document.getElementById(target).style.display = str;
    }
    $(document).ready(function () {
        //add fotos
        var n_foto = 0;
        var foto = 0;
        $('#add').click(function (event) {
            if (n_foto < 1) {
                $("#imagens").append("<div class='form-group'><label class='col-sm-3 control-label'>Selecione:</label><div class='col-sm-9'><div class='input-group'><span class='input-group-btn'><span class='btn btn-primary btn-file'>Browse&hellip; <input name='fileToUpload" + foto++ + "' type='file' accept='image/*' ></span></span><input type='text' class='form-control' readonly></div></div></div>");
                n_foto++;
                $("#nro_img").val(n_foto);
            }
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            $(document).ready(function () {
                $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log)
                            alert(log);
                    }

                });
            });
        });

    });
</script>
