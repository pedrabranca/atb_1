<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login Area Administrativa</title>
        <link rel="shortcut icon" href="http://mauricio.16mb.com/arquivos/img/logo.ico" />
        <link href="css/layout.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/lib/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <link href="../arquivos/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <style>
            html{
                height:100%;
                width:100%;
            }
            body {
                background-color: #eee;
                background-image: url("../arquivos/img/wallpaper.jpeg");
                background-repeat: no-repeat;
                background-size: 100% 120%;
            }
            input{
                display: block;
                width:90%;
                margin-bottom: 5px;
            }
            .container{
                max-width: 330px;
                padding: 15px;
                margin: 65px auto;
                background-color: #E4E4E4;
                border-radius: 5px;
            }.btn{
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div id="form_container" class='container-fluid'>
            <form action="../includes/controller/UsuarioController.php" method="post" name="frm_login">
                <div class="container">
                    <input type="hidden" name="acao" value="autenticar" />
                    <h3>Efetuar login:</h3>
                    <input type="text" name="login" id="login" placeholder="Login" autofocus required class='form-control'/>
                    <input type="password" name="senha" id="senha" placeholder="Senha" required class="form-control"/>
                    <div id="resultado"></div>
                    <button type="submit" name="btn_login" class="btn btn-primary btn-lg btn-block">Entrar</button>
                </div>
            </form>
        </div>
    </body>
</html>
