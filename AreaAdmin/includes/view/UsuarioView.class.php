<?php

class UsuarioView {

    function respostaAutenticacao($resposta) {

        /* Se a vari�vel $resposta estiver neste momento como TRUE, ent�o os dados est�o corretos e podemos
          exibir uma mensagem de sucesso. Caso contr�rio, ir� cair no else, que ir� alertar que os dados s�o inv�lidos. */
        if ($resposta) {
            echo '<p class="blue">Entrando...</p>';
            echo "<script>
                function entrar(){
                    window.location.replace('../home');
                }
                entrar();
                        </script>";
        } else {
            echo '<p class="red">Dados incorretos!</p>';
        }
    }

}
