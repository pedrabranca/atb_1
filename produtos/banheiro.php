<!DOCTYPE html>
<html lang="pt">

    <head>
        <link rel="icon" type="image/jpg" href="../arquivos/imagens/logo.jpg" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>ATB MOVÉIS</title>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/menu.css" rel="stylesheet">
        <link href="../css/footer.css" rel="stylesheet">
        <link href="../css/boxstyles.css" rel="stylesheet">

        <script src='../arquivos/js/bootstrap.min.js' type='text/javascript'></script>
        <script src='../arquivos/js/main.js' type='text/javascript'></script>
        <link rel='stylesheet' href='../arquivos/css/normal/fonts.css' type='text/css'/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <style type="text/css">
            body{
                background-image:url("../arquivos/imagens/background.jpg");
                margin-top: 0px !important;
                padding-top: 0px !important;
            }

            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                background: red;
                cursor: inherit;
                display: block;
            }
            input[readonly] {
                background-color: white !important;
                cursor: text !important;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class = "navbar-header">
                    <button type = "button" class = "navbar-toggle"
                            data-toggle = "collapse" data-target = "#example-navbar-collapse">
                        <span class = "sr-only">Toggle navigation</span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                        <span class = "icon-bar"></span>
                    </button>
                    <a id="logo"class="navbar-brand" href="Home">
                        <img class="logo" alt="Logo Site" src="../arquivos/imagens/logo.png" />
                        <p class="navbar-brand"></p></a>
                </div>
                <div class = "collapse navbar-collapse" id = "example-navbar-collapse">

                    <ul class = "nav navbar-nav">
                        <li class = ""><a href = "index.html">Pagina Inicial</a></li>
                        <li><a class=""href = "index.html">Produtos</a></li>
                        <li><a href = "serviços.html">Serviços</a></li>
                        <li><a href = "contato.html">Contato</a></li>
                        <li><a href = "sobre.html">Sobre nós</a></li>
                    </ul>
                </div>
        </nav>
        <!-- Page Content -->
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example" data-slide-to="1"></li>
                <li data-target="#carousel-example" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <a href="#"><img class="slide-img" src="../arquivos/imagens/logo.jpg" /></a>
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <a href="#"><img class="slide-img" src="../arquivos/imagens/teste.jpg" /></a>
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <a href="#"><img class="slide-img" src="../arquivos/imagens/logo.jpg" /></a>
                    <div class="carousel-caption">

                    </div>
                </div>
            </div>

            <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>

        <div class="container">
            <hr>
            <!-- Title -->
            <?php
            include_once '../AreaAdmin/includes/model/dao/bd2.php';

            $pdo = conecta();

            $busca = $pdo->prepare("SELECT * FROM img_banheiro order by id desc;");
            $busca->execute();
            if ($busca->rowCount() == 0) {
                echo "<h1 style='text-align:center;margin:50px;'>Não há produtos nesta categoria</h1>";
            } else {
                while ($linha = $busca->fetch(PDO::FETCH_ASSOC)) {
                    $caminho = '../AreaAdmin/arquivos/img/banheiro/';
                    //echo " <div class='col-sm-2' style='background-color:blue; float:left'></div> <div style='background-color:red;' class='col-sm-8' >asldkfj</div> <div class='col-sm-2' style='background-color:green;float:right'></div> ";
                    echo "<div class='row'>
  <div class='col-md-6 col-md-offset-3' style='margin-top:20px;'><img src='" . $caminho . $linha['img1'] . "' style='width:100%; height: 400px; border: 2px solid black;'></div>
</div>";
                    //echo "<h1>LINHA</h1>";
                    //echo $linha['descricao'];
                }
            }
            ?>
            <br /><br />
            <!-- Footer -->
            <div class="push"></div>
        </div>


        <div class="row-center1">
            <div class="col-md-12">

            </div>
        </div>

        <!-- /.container -->
    </div>
    <footer id="footerWrapper">
        <section id="mainFooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="footerWidget">
                            <img src="../arquivos/imagens/logo.png" alt="" id="footerLogo">
                            <p><a href="#" title="Em breve, nome da nossa empresa xd"></a>  </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footerWidget">

                            <h3>ATB MOVEIS</h3>
                            <address>
                                <p> <i class="icon-location"></i>&nbsp;Nilton Vaz Cachapuz 115<br>
                                    <i class="icon-location"></i>&nbsp;Bagé, RS Brasil<br>
                                    <i class="icon-phone"></i>&nbsp;615.987.1234 <br>
                                    <i class="icon-mail-alt"></i>&nbsp;<a href="#"></a> </p>
                            </address>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footerWidget">
                            <h3>Nossos Números :</h3>
                            <p> <i class="icon-location"></i>&nbsp;995454594<br>
                                <i class="icon-location"></i>&nbsp;32416567<br>
                                <i class="icon-phone"></i>&nbsp;43434343 <br>
                                <i class="icon-mail-alt"></i>&nbsp;<a href="#"></a> </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <i class="icon-location"></i>&nbsp;<img src="../arquivos/imagens/nossologo.png" alt="" id="footerLogonosso"><br>
                        <i class="icon-location"></i>&nbsp;
                        <button id="botao" type="submit" class="btn btn-secondary" onclick="window.open('../AreaAdmin/')">Login</button><br>

                    </div>
                </div>
            </div>
        </section>
        <section id="footerRights">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Copyright © 2016 <a id="atb" href="#" target="blank">ATB</a> / All rights reserved.</p>
                    </div>

                </div>
            </div>
        </section>
    </footer>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
